//
//  DetailViewController.swift
//  CoreDataHomework4
//
//  Created by Sophealey on 12/20/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var noteDetailTextView: UITextView!
    
    //core data
    var notes:[PersonalNote]?
    let context  = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var noteIndex:Int?
    var titleNote:String?
    var note:String?
    var status:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        noteDetailTextView.delegate = self
        if status == "edit"{
            self.titleTextField.placeholder = "Title"
            self.titleTextField.text = ((titleNote != "Untitle") ? titleNote : "" ) ?? ""
            if note != "" {
                 self.noteDetailTextView.text = note
            }else{
                noteDetailTextView.text = "Note"
                noteDetailTextView.textColor = UIColor.lightGray
            }
            
        }else{
            noteDetailTextView.text = "Note"
            noteDetailTextView.textColor = UIColor.lightGray
        }
        
        //Mark: work with auto scroll when keyboard is present
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)

    }
    //Mark: textView Placehodler
    func textViewDidBeginEditing(_ textView: UITextView) {
        if noteDetailTextView.textColor == UIColor.lightGray {
            noteDetailTextView.text = nil
            noteDetailTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if noteDetailTextView.text.isEmpty {
            noteDetailTextView.text = "Note"
            noteDetailTextView.textColor = UIColor.lightGray
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if status == "edit"{
            editNote()
        }else {
            
                addNote()

        }
    }

    func addNote(){
        if titleTextField.text == "" && noteDetailTextView.textColor == UIColor.lightGray{
            print("No note to save")
        }else{
            let note = PersonalNote(context: context)
            note.title =  (titleTextField.text != "") ? titleTextField.text : "Untitle"
            note.note = noteDetailTextView.text
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
       
    }
    func editNote(){
        notes = try? context.fetch(PersonalNote.fetchRequest())
        notes?.reverse()
        notes![noteIndex!].title = (titleTextField.text != "" ) ? titleTextField.text : "Untitle"
        notes![noteIndex!].note = (noteDetailTextView.textColor != UIColor.lightGray) ? noteDetailTextView.text : ""
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }

    @IBAction func moreBarButtonItem(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: "Delete", style: .default) { (action) in
            print(action)
        }
        let copy = UIAlertAction(title: "Make a copy", style: .default) { (action) in
            print(action)
        }
        let send = UIAlertAction(title: "Send", style: .default) { (action) in
            print(action)
        }
        let Collaborates = UIAlertAction(title: "Collaborates", style: .default) { (action) in
            print(action)
        }
        let labels = UIAlertAction(title: "Labels", style: .default) { (action) in
            print(action)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print(action)
        }
        alert.addAction(delete)
        alert.addAction(copy)
        alert.addAction(send)
        alert.addAction(Collaborates)
        alert.addAction(labels)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addBarButtonItem(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take photo", style: .default) { (action) in
            print(action)
        }
        let chooseImage = UIAlertAction(title: "Choose image", style: .default) { (action) in
            print(action)
        }
        let drawing = UIAlertAction(title: "Drawing", style: .default) { (action) in
            print(action)
        }
        let recording = UIAlertAction(title: "Recording", style: .default) { (action) in
            print(action)
        }
        let checkboxes = UIAlertAction(title: "Checkboxes", style: .default) { (action) in
            print(action)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print(action)
        }
        alert.addAction(takePhoto)
        alert.addAction(chooseImage)
        alert.addAction(drawing)
        alert.addAction(recording)
        alert.addAction(checkboxes)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    //Mark: auto scroll when keyboard is present
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            noteDetailTextView.contentInset = UIEdgeInsets.zero
        } else {
            noteDetailTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        noteDetailTextView.scrollIndicatorInsets = noteDetailTextView.contentInset
        
        let selectedRange = noteDetailTextView.selectedRange
        noteDetailTextView.scrollRangeToVisible(selectedRange)
    }
    
}
