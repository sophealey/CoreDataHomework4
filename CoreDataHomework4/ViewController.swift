//
//  ViewController.swift
//  CoreDataHomework4
//
//  Created by Sophealey on 12/18/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var NoteCollectionView: UICollectionView!
    
    var notes:[PersonalNote]?
    let context  = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.getData()
            self.NoteCollectionView.reloadData()
        }
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
        
        //set delegate and datasource
        NoteCollectionView.delegate = self
        NoteCollectionView.dataSource = self
        
        //Set Prefer Large Title
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.topItem?.title = "Notes"
        navigationController?.navigationItem.largeTitleDisplayMode = .always
        
        //set items layout
        let layout = self.NoteCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: (self.view.frame.size.width - 30)/2, height: self.view.frame.size.height/4)
        
        //Mark: long hold
        let holdToDelete : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:)))
        holdToDelete.minimumPressDuration = 0.5
        holdToDelete.delegate = self as? UIGestureRecognizerDelegate
        holdToDelete.delaysTouchesBegan = true
        self.NoteCollectionView?.addGestureRecognizer(holdToDelete)
   
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (notes?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteCell", for: indexPath) as! NoteCollectionViewCell
        cell.configureCell(title: notes![indexPath.row].title!, note: notes![indexPath.row].note!)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let noteDetail = self.storyboard?.instantiateViewController(withIdentifier: "noteDetailStoryboard") as! DetailViewController
        
        noteDetail.titleNote = notes![indexPath.row].title
        noteDetail.note = notes![indexPath.row].note
        notes?.reverse()
        noteDetail.noteIndex = indexPath.row
        noteDetail.status = "edit"
        self.navigationController?.pushViewController(noteDetail, animated: true)
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        let p = gesture.location(in: self.NoteCollectionView)
        
        if let indexPath = self.NoteCollectionView.indexPathForItem(at: p) {
            //            let cell = self.noteCollectionView.cellForItem(at: indexPath) as! NoteCell
            let alert = UIAlertController(title: "Are you sure to delete this note?", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {action in
                let note = self.notes?[indexPath.row]
                self.context.delete(note!)
                self.appDelegate.saveContext()
                self.getData()
                self.NoteCollectionView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true)
        } else {
            print("couldn't find index path")
        }
    }
    
    func getData(){
        notes = try? context.fetch(PersonalNote.fetchRequest())
        notes?.reverse()
    }
    @IBAction func addNewNoteAction(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "addNew", sender: nil)
    }
    
}

