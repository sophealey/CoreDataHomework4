//
//  NoteCollectionViewCell.swift
//  CoreDataHomework4
//
//  Created by Sophealey on 12/19/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var noteLabel: UILabel!
    
    func configureCell(title:String,note:String){
        self.titleLabel.text = title
        self.noteLabel.text = note
    }
}
